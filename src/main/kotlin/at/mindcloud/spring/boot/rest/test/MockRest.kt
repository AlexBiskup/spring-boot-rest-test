package at.mindcloud.spring.boot.rest.test

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import at.mindcloud.spring.boot.rest.test.RequestType.*

@Component
class MockRest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    lateinit var objectMapper: ObjectMapper

    inline fun <reified T> post(request: Request): T? {
        val response = performRequest(request, POST)
        return mapResponseToObject(response)
    }

    inline fun <reified T> get(request: Request): T? {
        val response = performRequest(request, GET)
        return mapResponseToObject(response)
    }

    fun performRequest(request: Request, type: RequestType): MockHttpServletResponse {
        val url = request.url
        val requestBuilder = when (type) {
            GET -> MockMvcRequestBuilders.get(url)
            POST -> MockMvcRequestBuilders.post(url)
            PUT -> MockMvcRequestBuilders.put(url)
            DELETE -> MockMvcRequestBuilders.delete(url)
        }
        applyHeadersToRequestBuilder(request.headers, requestBuilder)
        applyContentToRequestBuilder(request.content, requestBuilder)
        return performAndExpectStatus(requestBuilder, request.expectedStatus)
    }

    private fun applyHeadersToRequestBuilder(headers: List<Header>, requestBuilder: MockHttpServletRequestBuilder) {
        headers.forEach { requestBuilder.header(it.name, it.value) }
    }

    private fun applyContentToRequestBuilder(content: Any?, requestBuilder: MockHttpServletRequestBuilder) {
        if(content != null) {
            requestBuilder.contentType(CONTENT_TYPE)
            requestBuilder.content(objectMapper.writeValueAsString(content))
        }
    }

    private fun performAndExpectStatus(requestBuilder: MockHttpServletRequestBuilder, expectedStatus: HttpStatus): MockHttpServletResponse {
        return mockMvc.perform(requestBuilder).andExpect(status().`is`(expectedStatus.value())).andReturn().response
    }

    inline fun <reified T> mapResponseToObject(response: MockHttpServletResponse): T? {
        val content = response.contentAsString
        if(content.isEmpty() || content.isBlank()) {
            return null
        }
        return objectMapper.readValue(response.contentAsString, T::class.java)
    }
}

private const val CONTENT_TYPE = "application/json"

data class Request(
    val url: String,
    val expectedStatus: HttpStatus,
    val content: Any? = null,
    val headers: List<Header> = listOf()
)

data class Header(
    val name: String,
    val value: String
)

enum class RequestType {
    GET, POST, PUT, DELETE
}
